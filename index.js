
import path from "path"

import File from "./file.js"
import Page from "./page.js"
import PageList from "./page_list.js"
import Controls from "./controls.js"
import Globals from "./globals/index.js"

import fs from 'fs'

import nunjucks from "nunjucks"

import less from "less"
import csso from 'csso'

import { URL } from 'url'; // in Browser, the URL in native accessible on window

const __filename = new URL('', import.meta.url).pathname;
// Will contain trailing slash
const __dirname = new URL('.', import.meta.url).pathname;

export default class PagesIO {

  constructor(app, cfg) {
    this.router = app;
    if (this.router) this.router.pages_io = this;

    this.aliases = cfg.aliases;


    this.list = [];
    this.page_lists = [];
    this.globals = [];

    this.cfg = cfg;

    this.extra_context = cfg.extra_context;

    this.app_path = cfg.app_path;
    this.admin_path = cfg.admin_path;

    if (app) {
      app.get("/ck-lang", function(req, res) {
        res.json({ cur: req.cookies["lang"]});
      });

      app.get("/set", function(req, res) {
        if (req.query.lang) {
          let lang = req.query.lang;
          
          res.cookie('lang', lang, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24
          });

          res.send(nunjucks.renderString(fs.readFileSync(path.resolve(__dirname, 'set-lang.html'), "utf8"), { lang: lang }));
        }
      });
    }

    let this_class = this;

    function read_lang_json(tag) {
      if (this_class.cfg.globals_path) {
        let jpath = path.resolve(this_class.cfg.globals_path, "lang", tag+".json");
        if (fs.existsSync(jpath)) return JSON.parse(fs.readFileSync(jpath, "utf8"));
      }
      return undefined;
    }

    this.langs = {
      lt: read_lang_json("lt"),
      en: read_lang_json("en")
    }

    if (!this.cms) this.cms = {};
    if (cfg.dev_mode) this.cms.dev_mode = cfg.dev_mode;

    this.dev_mode = this.cms.dev_mode;
    if (app) {
      this.cms = app.cms;
    }
  }

  static async init(app, cfg) {
    try {

      return new PagesIO(app, cfg);
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }

  }

  init_controls(auth, cms) {
    this.router.cms = cms;
    this.controls = new Controls({
      command_path: this.admin_path+"/pages.io",
      auth: auth
    }, cms);
  }

  watch_less(file) {
    /*
    let _this = this;
    function render_less() {
      if (fs.existsSync(file)) {
        less.render(fs.readFileSync(file, 'utf8'), {}, function(err, output) {
          let report_time = new Date();
          report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);
          if (err) {
            console.error("");
            console.error("\x1b[34m"+report_time+" \x1b[31mLESS COMPILATION ERROR:")
            console.error("\x1b[33m"+err.message+" --- \x1b[32mline: "+err.line+"\x1b[0m");
            console.error("\x1b[36m");
            for (let e = 0; e < err.extract.length; e++) {
              if (err.extract[e]) console.error(err.extract[e]);
            }
            console.error("\x1b[0m");
          } else {
            let output_css = output.css;
            if (!_this.dev_mode) output_css = csso.minify(output_css).css;
            fs.writeFileSync(file+".css", output_css);
            console.log("\x1b[34m"+report_time, "\x1b[32mLESS ----->\x1b[36m", file+".css\x1b[0m");
          }
        });
      }
    }

    fs.watchFile(file, { interval: 1000 }, function(curr, prev) {
//      console.log(`the current mtime is: ${curr.mtime}`);
//      console.log(`the previous mtime was: ${prev.mtime}`);
      render_less() 
    });
    
    render_less()
    */
  }

  serve_file(request_path, file_path, cfg) {
    let new_file = new File(this.router, request_path, file_path, cfg);
  }

  serve_dir(request_path, dir_full_path, cfg) {
    let page_cfg = {
      request_path: request_path,
      full_path: dir_full_path,
      app_path: this.app_path,
      cms_path: this.cfg.cms_path,
      modules_path: path.resolve(this.app_path, "mellisuga_modules"),
      disabled_pages: cfg.disabled_pages,
      host: this.cfg.host,
      port: this.cfg.port,
      autowatch: this.cfg.autowatch,
      globals_path: cfg.globals_path || this.cfg.globals_path,
      aliases: this.aliases,
      pages_io: this,
      parent_list: cfg.parent_list
    }

    if (cfg) {
      if (cfg.aliases) { 
        for (let alias in cfg.aliases) {
          page_cfg.aliases[alias] = cfg.aliases[alias];
        }
      }

      page_cfg.auth = cfg.auth;
      page_cfg.name = cfg.name;
      page_cfg.dev_only = cfg.dev_only;
      page_cfg.required_rights = cfg.required_rights;
      page_cfg.context = cfg.context || {};
      let rdy_lang = {};
      for (let lang in this.langs) {
        rdy_lang[lang] = this.langs[lang];
        if (cfg.extras && cfg.extras.lang_path) {
          let lang_path = path.resolve(cfg.extras.lang_path, lang+".json");
          if (fs.existsSync(lang_path)) {
            rdy_lang[lang] = { ...rdy_lang[lang], ...JSON.parse(fs.readFileSync(
              lang_path,
              "utf8"
            )) };
          }
        }
      }
      page_cfg.lang = rdy_lang;
    }

    let new_page = new Page(this.router, page_cfg);
    let page_reused = false;
    for (let p = 0; p < this.list.length; p++) {
      let existing_page = this.list[p];
      if (existing_page.full_path == new_page.full_path) page_reused = true;
    }
    if (!page_reused) this.list.push(new_page);
    return new_page;
  }

  serve_dirs(request_path_prefix, dir_full_path, cfg) {
    let reused_page_list = false;
    for (let l = 0; l < this.page_lists.length; l++) {
      let page_list = this.page_lists[l];
      if (page_list.full_path == dir_full_path) reused_page_list = true;
    }

    let page_list_cfg = {
      req_prefix: request_path_prefix,
      full_path: dir_full_path,
      app_path: this.app_path,
      cms_path: this.cfg.cms_path,
      host: this.cfg.host,
      port: this.cfg.port,
      autowatch: this.cfg.autowatch,
      globals_path: cfg.globals_path || this.cfg.globals_path,
      aliases: {...this.aliases},
      pages_io: this,
      reused: reused_page_list,
      dev_mode: this.dev_mode
    }

    if (cfg) {
      if (cfg.aliases) { 
        for (let alias in cfg.aliases) {
          page_list_cfg.aliases[alias] = cfg.aliases[alias];
        }
      }
//      console.log("SERVE DIRS", request_path_prefix, page_list_cfg.aliases);
      page_list_cfg.auth = cfg.auth;
      page_list_cfg.name = cfg.name;
      page_list_cfg.dev_only = cfg.dev_only || this.dev_mode;
      page_list_cfg.required_rights = cfg.required_rights;
      page_list_cfg.global_context_path = cfg.global_context_path;
      page_list_cfg.context = cfg.context || {};
      if (this.extra_context) {
        for (let key in this.extra_context) {
          let extra = this.extra_context[key];
          if (typeof extra === "function") {
            page_list_cfg.context[key] = extra();
          } else {
            page_list_cfg.context[key] = extra;
          }
        }
      }
      page_list_cfg.custom_names = cfg.custom_names;
      page_list_cfg.disabled_pages = cfg.disabled_pages;
      page_list_cfg.root_page = cfg.root_page;
      page_list_cfg.context.menu_prefix = request_path_prefix;


      let rdy_lang = {};
      for (let lang in this.langs) {
        rdy_lang[lang] = this.langs[lang];
        if (cfg.extras && cfg.extras.lang_path) {
          let lang_path = path.resolve(cfg.extras.lang_path, lang+".json");
          if (fs.existsSync(lang_path)) {
            rdy_lang[lang] = { ...rdy_lang[lang], ...JSON.parse(fs.readFileSync(
              lang_path,
              "utf8"
            )) };
          }
        }
      }
      page_list_cfg.lang = rdy_lang;
    }

    let new_page_list = new PageList(page_list_cfg, this.router);

    if (!reused_page_list) this.page_lists.push(new_page_list);
    return new_page_list;
  }

  serve_globals(url_path, dirname, cfg) {
    if (!cfg) cfg = {};
    if (!cfg.app_path) cfg.app_path = this.app_path;
    if (!cfg.globals_path) cfg.globals_path = this.globals_path; // TODO check this out. Possibly need to set this to dirname
    if (!cfg.modules_path) cfg.modules_path = path.resolve(this.app_path, "mellisuga_modules");
    if (!cfg.dev_mode) cfg.dev_mode = this.dev_mode;
    if (!cfg.aliases) cfg.aliases = this.cfg.aliases;

    let exists = false;
    for (let g = 0; g < this.globals.length; g++) {
      if (this.globals[g].dirname == dirname) {
        exists = this.globals[g];
      }
    }

    if (!exists) {
      let global_obj = new Globals(this.router, url_path, dirname, this, cfg);
      this.globals.push(global_obj);
      return global_obj;
    } else if (cfg.page_list) {
      exists.page_lists.push(cfg.page_list);
      return exists;
    }
  }

//  - -                    - -
// - - - SELECTOR METHODS - - -
//  - -                    - -

  all() {
    let obj = {};

    for (let l = 0; l < this.page_lists.length; l++) {
      if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
        obj[this.page_lists[l].name] = {
          dirname: this.page_lists[l].full_path,
          path: this.page_lists[l].req_prefix,
          pages: [],
//          globals: [this.page_lists[l].globals.data()]
        };

        for (let p = 0; p < this.page_lists[l].list.length; p++) {
          obj[this.page_lists[l].name].pages.push(this.page_lists[l].list[p].data());
        }
      }
    }

    obj.unlisted = {
      pages: [],
//      globals: []
    };
    for (let p = 0; p < this.list.length; p++) {
      if (!this.list[p].dev_only || this.list[p].dev_only && this.cms.dev_mode) {
        obj.unlisted.pages.push(this.list[p].data());
      }
    }
/*
    for (let g = 0; g < this.globals.length; g++) {
      obj.unlisted.globals.push(this.globals[g].data());
    }
*/
    return obj;
  }

  all_globals() {
    let out = [];

    for (let g = 0; g < this.globals.length; g++) {
      out.push(this.globals[g].data());
    }

    return out;
  }
/*
  select(page_name) {
    for (let p = 0; p < this.list.length; p++) {
      let pdata = this.list[p].data();
      if (pdata.name === page_name) {
        return pdata;
      }
    }

    for (let l = 0; l < this.page_lists.length; l++) {
      if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
        for (let p = 0; p < this.page_lists[l].list.length; p++) {
          let pdata = this.page_lists[l].list[p].data();
          if (pdata.name === page_name) {
            return pdata;
          }
        }
      }
    }

    return undefined;
  }
*/
  all_from_list(list_name) {
    for (let l = 0; l < this.page_lists.length; l++) {
      if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
        if (this.page_lists[l].name === list_name) {
          return this.page_lists[l].all();
        }
      }
    }

    return undefined;
  }

  select_from_list(list_name, page_name) {
    for (let l = 0; l < this.page_lists.length; l++) {
      if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
        if (this.page_lists[l].name === list_name) {
          return this.page_lists[l].select(page_name);
        }
      }
    }
    return undefined;
  }

  select_obj(list_name, page_name) {
    if (list_name === "unlisted") {
      for (let p = 0; p < this.list.length; p++) {
        if (!this.list[p].dev_only || this.list[p].dev_only && this.cms.dev_mode) {
          let pdata = this.list[p];
          if (pdata.name === page_name) {
            return pdata;
          }
        }
      }
    } else {
      for (let l = 0; l < this.page_lists.length; l++) {
        if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
          if (this.page_lists[l].name === list_name) {
            for (let p = 0; p < this.page_lists[l].list.length; p++) {
              let pdata = this.page_lists[l].list[p];
              if (pdata.name === page_name) {
                return pdata;
              }
            }
          }
        }
      }
    }
    return undefined;
  }

  select_list_obj(list_name) {
    for (let l = 0; l < this.page_lists.length; l++) {
      if (!this.page_lists[l].dev_only || this.page_lists[l].dev_only && this.cms.dev_mode) {
        if (this.page_lists[l].name === list_name) {
          return this.page_lists[l];
        }
      }
    }
    return undefined;
  }
}
