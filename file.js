
import fs from "fs"

export default class {

  constructor(app, req_path, file_path, cfg) {
    this.app = app;
    this.auth_func = cfg.auth_func;
    this.req_path = req_path;
    this.file_path = file_path;
    this.serve();
  }

  serve() {
    let _this = this;
    console.log("serve", this.req_path);
    if (this.auth_func) {
      this.app.get(this.req_path, this.auth_func, async function(req, res) {
     //   let filedata = fs.readFileSync(_this.file_path, "base64");
     //    res.set({ 'content-type': 'application/pdf; charset=base64' });
     //   res.send(filedata);
        res.sendFile(_this.file_path);
      });
    } else {
      this.app.get(this.req_path, async function(req, res) {
        res.sendFile(_this.file_path);
      });
    }
  }
}
