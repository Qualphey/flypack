
import express from "express"

import Pack from "../pack/index.js"


export default class {

  constructor(app, url_path, dirname, pages_io, cfg) {
    if (url_path && dirname) {

      let _this = this;
      this.cfg = cfg;
      this.path = url_path;
      this.dirname = dirname;
      this.page_lists = [];

      if (cfg.page_list) this.page_lists.push(cfg.page_list);
      cfg.global = true;

      if (app) {
        app.use(
          url_path,
          express.static(dirname)
        );
      }

      this.pack = new Pack(dirname, pages_io, cfg);
    } else {
      if (!url_path) console.error(new Error("express-pagepack can't construct globals class without the `url_path` argument"));
      if (!dirname) console.error(new Error("express-pagepack can't construct globals class without the `dirname` argument"));
    }
  }

  data() {
    let page_lists = [];
    for (let p = 0; p < this.page_lists.length; p++) {
      page_lists.push(this.page_lists[p].name);
    }

    return {
      dirname: this.dirname,
      path: this.path,
      page_lists: page_lists,
      watch: this.pack.watch_state
    }
  }
}
