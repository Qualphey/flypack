import less from 'less'

export default function(content) {
	this.cacheable && this.cacheable();
	this.value = content;

  let injection = "let style = document.createElement('style');";
  injection += "style.innerHTML = '"+content.replace(/(\r\n\t|\n|\r\t)/gm,"").replace(/'/g, '"')+"';";
  injection += "document.head.appendChild(style);";

	return injection;
}
