
import nunjucks from 'nunjucks'

import fs from "fs-extra"
import path from 'path'

import webpack from 'webpack'

import chokidar from 'chokidar'

import cookie from 'cookie'

import Pack from './pack/index.js'


const api_get = async (req_url) => {
  try {
    return await new Promise((out) => {
      http.get(req_url, (res) => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];

        let error;
        // Any 2xx status code signals a successful response but
        // here we're only checking for 200.
        if (statusCode !== 200) {
          error = new Error('Request Failed.\n' +
                            `Status Code: ${statusCode}`);
        } else if (!/^application\/json/.test(contentType)) {
          error = new Error('Invalid content-type.\n' +
                            `Expected application/json but received ${contentType}`);
        }
        if (error) {
          console.error(error.message);
          // Consume response data to free up memory
          res.resume();
          out();
          return;
        }

        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => {
          try {
            const parsedData = JSON.parse(rawData);
            out(parsedData);
          } catch (e) {
            console.error(e.message);
          }
        });
      }).on('error', (e) => {
        console.error(`Got error: ${e.message}`);
        out();
      });
    });
  } catch (e) {
    console.error(e.stack);
  }
}


function merge_objects(obj1, obj2) {
  for (let pname in obj2) {
    if (!obj1[pname]) {
      obj1[pname] = obj2[pname]
    } else {
      if (typeof obj1[pname] !== typeof obj2[pname]) console.log("Error: inconsistent object property format");
      if (typeof obj1[pname] === "object" && obj1[pname] !== null && obj2[pname] !== null) { // MERGE SUB OBJECTS
        if (Array.isArray(obj1[pname])) { // MERGE ARRAYS
          obj1[pname] = [ ...obj1[pname], ...obj2[pname] ]
        } else {
          merge_objects(obj1[pname], obj2[pname]);
        }
      } 
    }
  }

  return obj1;
}


/**
 * Represents a directory hosted as a page with all its resources
 * @class Page
 * @arg {Object} app Express app
 * @arg {Object} cfg Configuration
 * @arg {String} cfg.full_path Full path to the page directory
 */
export default class {
  constructor(app, cfg) {
    this.app = app
    this.cfg = cfg;

    this.dev_mode = cfg.pages_io.dev_mode;

    if (app) {
      this.disable_super = app.cms.disable_super;

      this.cms = app.cms;
    }

    this.context_extras = [];

    this.full_path = cfg.full_path || path.resolve(cfg.dir_path, cfg.name);
    this.rel_path = "./"+cfg.full_path.replace(cfg.app_path+'/', '');
    this.http_path = cfg.custom_path || cfg.request_path || (cfg.prefix ? path.resolve(cfg.prefix, encodeURIComponent(cfg.name)) : undefined);


    if (cfg.name) {
      this.name = cfg.name;
    } else {
      this.name = this.full_path.substr(this.full_path.lastIndexOf('/') + 1);
    }



    this.pages_io = cfg.pages_io;

//    if (!this.disable_super) this.pages_io.watch_less(path.resolve(this.full_path, "theme.less"))

    this.custom_context = cfg.context;
 
    this.dev_only = cfg.dev_only;

    let index_path = this.index_path = path.resolve(this.full_path, "index.html");
    let context_path = this.context_path = path.resolve(this.full_path, "context.json");
    const globals_path = cfg.globals_path;
    const modules_path = cfg.modules_path;

    let global_context_path = this.global_context_path = path.resolve(
      globals_path, cfg.global_context_path || "context.json"
    );

    this.root_page = cfg.root_page;

    this.update();

    this.req_path = cfg.request_path;

    if (!this.req_path) this.req_path = "";

    this.custom_paths = cfg.custom_paths;

    this.disabled_pages = cfg.disabled_pages;

    cfg.dir_path = cfg.dir_path || cfg.full_path;

    this.auth = cfg.auth;

   //` console.log("PAGE", this.full_path, this.context);
    if (this.context) {
      if ((this.context.authorized_only || this.context.authorize || this.context.protected || (this.context.auth && this.context.auth.protected)) && cfg.auth) {
        this.auth_func = cfg.auth.orize;
      } else if (this.context.required_rights && cfg.auth) {
        this.auth_func = cfg.auth.orize_gen(this.context.required_rights);
      } else if (this.context.auth && this.context.auth.use_session && !(this.pure_context && this.pure_context.auth && this.pure_context.auth.use_session === false) && cfg.auth) {
        this.auth_func = cfg.auth.use_session;
      }
    }
/*
    let midd_path = path.resolve(this.full_path, "middleware", "index.js");
    this.middleware = fs.existsSync(midd_path) ? require(midd_path) : undefined;

    let g_midd_path = path.resolve(cfg.globals_path, "middleware", "index.js");
    this.global_middleware = fs.existsSync(g_midd_path) ? require(g_midd_path) : undefined;
*/
    if (cfg.auth && cfg.required_rights) {

      this.auth_func = cfg.auth.orize_gen(cfg.required_rights);
    }

 //   console.log(this.auth_func);

    this.parent_list = cfg.parent_list;

    if (cfg.custom_path && app) {
      app.get(cfg.request_path, function(req, res) {
        res.redirect(cfg.custom_path);
      });
    }

    let NJKLoader = {
      async: true,
      getSource: function(name, callback) {
        if (name.startsWith("/g/")) {
          (async function() {
            try {
              let tmpl_src = fs.readFileSync(path.resolve(cfg.app_path, "globals" , name.slice(3)));
              callback(false, {
                src: tmpl_src
              });
            } catch (e) {
              console.error(e.stack);
            }
          })();
        }
      }
    };

    this.nunjucks_env = new nunjucks.Environment(NJKLoader);

    /**
     * Returns a JSON stringified version of the value, safe for inclusion in an
     * inline <script> tag. The optional argument 'spaces' can be used for
     * pretty-printing.
     *
     * Output is NOT safe for inclusion in HTML! If that's what you need, use the
     * built-in 'dump' filter instead.
     */
    this.nunjucks_env.addFilter('json', function (value, spaces) {
      if (value instanceof nunjucks.runtime.SafeString) {
        value = value.toString()
      }
      const jsonString = JSON.stringify(value, null, spaces).replace(/</g, '\\u003c').split(String.fromCharCode(92)).join("&#92;");
      return nunjucks.runtime.markSafe(jsonString); 
    })

    this.pack = new Pack(this.full_path, this.pages_io, {
      app_path: cfg.app_path,
      globals: cfg.globals_path,
      modules_path: cfg.modules_path,
      dev_mode: this.dev_mode,
      aliases: cfg.aliases,
      page_list: this.parent_list,
      reused: cfg.reuse,
      dev_mode: cfg.dev_mode
    });

    if (app) this.serve_directory();
  }

  update() {
    this.context = false;
    if (fs.existsSync(this.context_path)) {
      try {
        this.pure_context = JSON.parse(fs.readFileSync(this.context_path, 'utf8'));
        this.context = {...this.pure_context};
      } catch (e) {
        this.pure_context = {};
        this.context = {};
        console.error(e);
      }
    } else {
      this.context = {};
    }

    if (this.context.replace_html) this.index_path = path.resolve(this.full_path, this.context.replace_html);

    if (fs.existsSync(this.index_path)) {
      try {
        this.index_html = fs.readFileSync(this.index_path, 'utf8');
      } catch (e) {
        console.error(e);
      }
    }

    if (fs.existsSync(this.global_context_path)) {
      try {
        var global_context = JSON.parse(fs.readFileSync(this.global_context_path, 'utf8'));
        this.context = Object.assign(this.context, global_context);
      } catch (e) {
        console.error(e);
      }
    }

    for (let c = 0; c < this.context_extras.length; c++) {
      let ctx = this.context_extras[c];

   //   console.log(this.context);
      console.log("add_to_context", typeof ctx, ctx);
      this.context = merge_objects(this.context, ctx);

   //   console.log(this.context);
    }

//    console.log("parent_list", this.parent_list ? this.parent_list.name : false);

    if (this.parent_list) {
      for (let c = 0; c < this.parent_list.context_extras.length; c++) {
        let ctx = this.parent_list.context_extras[c];

     //   console.log(this.context);
        console.log("add_to_context (LIST)", typeof ctx, ctx);
        this.context = merge_objects(this.context, ctx);

     //   console.log(this.context);
      }
    }

  }


  webpack_watch() {
    this.pack.toggle_webpack_watch();
  }

  destroy() {
    this.destroyed = true;
    fs.removeSync(this.full_path);
  }

  data() {
    let parent_list = "unlisted";

    if (this.parent_list) {
      parent_list = this.parent_list.name;
    }

    return {
      path: this.req_path,
      name: this.name,
      watch: this.pack.watch_state,
      dirname: this.full_path
    }
  }

  add_extra_context(ctx) {
    this.context_extras.push(ctx);
  }
}
