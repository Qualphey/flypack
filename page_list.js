"use strict"

import fs from 'fs-extra'
import path from 'path'
import express from 'express'

import Page from './page.js'

const page_blacklist = [
  ".builtin"
]

import { URL } from 'url'; // in Browser, the URL in native accessible on window

const __filename = new URL('', import.meta.url).pathname;
// Will contain trailing slash
const __dirname = new URL('.', import.meta.url).pathname;

var default_html = fs.readFileSync(__dirname+'/default_templates/index.html', 'utf8');
var default_json = fs.readFileSync(__dirname+'/default_templates/context.json', 'utf8');
var default_css = fs.readFileSync(__dirname+'/default_templates/theme.css', 'utf8');
var default_js = fs.readFileSync(__dirname+'/default_templates/main.js', 'utf8');

/**
 * Serves directory as a page list. For each subdirectory a {@link Page} class is created. 
 * @class PageList
 * @arg {Object} cfg Configuration
 * @arg {Object} app Express app
 */
export default class {
  constructor(cfg, app) {
    this.cfg = cfg;
    this.context = cfg.context;

    this.blacklist = page_blacklist

    this.context_extras = [];

    this.req_prefix = cfg.req_prefix;
    if (this.req_prefix) {
      while (!this.req_prefix.endsWith("/")) {
        this.req_prefix += "/";
      }
    }
    this.full_path = cfg.full_path;
    while (this.full_path.endsWith("/") && this.full_path.length > 1) {
      this.full_path = this.full_path.slice(0, -1);
    }

    if (cfg.name) {
      this.name = cfg.name;
    } else {
      this.name = this.full_path.substr(this.full_path.lastIndexOf('/') + 1);
    }

    this.required_rights = cfg.required_rights;

    this.global_context_path = cfg.global_context_path;
    
    this.custom_names = cfg.custom_names;
  
    this.disabled_pages = cfg.disabled_pages;

    this.dev_only = cfg.dev_only;
    this.dev_mode = cfg.dev_mode;

    this.root_page = cfg.root_page;

    this.auth = cfg.auth;

    if (!fs.existsSync(this.full_path)){
      fs.mkdirSync(this.full_path);
    }

    this.list = [];

    let this_class = this;

    this.globals_path = cfg.globals_path;

    this.config = {};
    const config_path = path.resolve(this.full_path, ".config.json");
    if (fs.existsSync(config_path)) {
      try {
        this.config = JSON.parse(fs.readFileSync(config_path, 'utf8'));
      } catch(e) {
        console.error(e.stack);
      }
    }

    if (cfg.globals_path) {
      this.globals = cfg.pages_io.serve_globals(this_class.req_prefix+'g', cfg.globals_path, {
        page_list: this
      });
/*      app.use(
        this_class.req_prefix+'g',
        express.static(cfg.globals_path)
      );*/
    }

    this.load().forEach(function(page) {
      var custom_path = false;

      if (this_class.config.custom_paths) {
        this_class.config.custom_paths.forEach(function(cpath) {
          const cpath_name = Object.keys(cpath)[0];
          if (page.file == cpath_name) {
            custom_path = cpath[cpath_name];
          }
        });
      }

      let npage = new Page(app, {
        app_path: cfg.app_path,
        cms_path: cfg.cms_path,
        request_path: this_class.req_prefix+encodeURIComponent(page.req_name),
        full_path: path.resolve(this_class.full_path, page.file),
        custom_path: custom_path,
        auth: this_class.auth,
        parent_list: this_class,
        globals_path: this_class.globals_path,
        required_rights: this_class.required_rights,
        global_context_path: this_class.global_context_path,
        context: this_class.context,
        root_page: this_class.root_page,
        modules_path: path.resolve(cfg.app_path, "mellisuga_modules"),
        disabled_pages: this_class.disabled_pages,
        host: cfg.host,
        port: cfg.port,
        autowatch: cfg.autowatch,
        globals_path: this_class.cfg.globals_path,
        lang: cfg.lang,
        aliases: cfg.aliases,
        pages_io: cfg.pages_io,
        reused: cfg.reused,
        dev_mode: cfg.dev_mode
      });

      this_class.list.push(npage);
    });
  }

  load() {
    var list = [];
    var this_class = this;

    fs.readdirSync(this.full_path).forEach(file => {
      var page = {};

      page.blacklisted = false;
      if (this_class.blacklist) {
        for (var b = 0; b < this_class.blacklist.length; b++) {
          if (file == this_class.blacklist[b]) {
            page.blacklisted = true;
          }
        }
      }
      if (!page.blacklisted) {
        var lstat = fs.lstatSync(path.resolve(this_class.full_path, file));
        if (lstat.isDirectory() || lstat.isSymbolicLink()) {
          page.path = '/'+file;
          if (this_class.config.custom_paths) {
            this_class.config.custom_paths.forEach(function(cpath) {
              const cpath_name = Object.keys(cpath)[0];
              const custom_path = cpath[cpath_name];
              if (file === cpath_name) {
                page.path = custom_path;
              }
            });

          }

          page.file = file;
          page.req_name = file;
          if (file === this_class.root_page) {
            page.req_name = '';
            page.root = true;

          }
    /*      if (this_class.custom_names) {
            if (this_class.custom_names[file]) page.req_name = this_class.custom_names[file];
          }*/
          page.name = decodeURIComponent(file);

          let disabled = false;
          if (this_class.disabled_pages && this_class.disabled_pages.includes(file)) {
            disabled = true;
          }
          if (!disabled) list.push(page);
        }
      }
    });

    list.sort(function(a, b) {
      return fs.statSync(path.resolve(this_class.full_path, a.file)).birthtime.getTime() - fs.statSync(path.resolve(this_class.full_path, b.file)).birthtime.getTime();
    });
    return list;
  }

  add(name, template) {
    let npage_path = path.resolve(this.full_path, name);
    if (!fs.existsSync(npage_path)) {
      var custom_path = false;
      if (this.config.custom_paths) {
        this.config.custom_paths.forEach(function(cpath) {
          const cpath_name = Object.keys(cpath)[0];
          if (name == cpath_name) {
            custom_path = cpath[cpath_name];
          }
        });
      }

      let npage_cfg = {
        app_path: cfg.app_path,
        cms_path: cfg.cms_path,
        request_path: this.req_prefix+encodeURIComponent(page.req_name),
        full_path: path.resolve(this.full_path, name),
        custom_path: custom_path,
        auth: this.auth,
        parent_list: this,
        globals_path: this.globals_path,
        required_rights: this.required_rights,
        root_page: this.root_page,
        lang: cfg.lang,
        aliases: cfg.aliases,
        disabled_pages: this.disabled_pages,
        reused: cfg.reused,
        dev_mode: cfg.dev_mode
      };



/*
      if (data.template && this_class.tamplate_dir) {
        var src_path = path.resolve(this_class.tamplate_dir, data.template);
        fs.copy(src_path, data.path, function (err) {
          if (err) return console.error(err)
          var npage = new Page(npage_cfg, cms, this_class);
          this_class.hosted_pages.push(npage);
          res.send(JSON.stringify({ msg: "success" }));
        });
      } else {*/
        fs.mkdirSync(npage_path);
        fs.writeFileSync(path.resolve(npage_path, "index.html"), default_html);
        fs.writeFileSync(path.resolve(npage_path, "context.json"), default_json);
        fs.writeFileSync(path.resolve(npage_path, "theme.css"), default_css);
        fs.writeFileSync(path.resolve(npage_path, "main.js"), default_js);
        var npage = new Page(app, npage_cfg);
        this.list.push(npage);
        return { msg: "success" };
    //  }
    } else {
      return { err: "Page `"+name+"` already exists!" };
    }
  }

  remove(name) {
    for (let p = 0; p < this.list.length; p++) {
      if (this.list[p].name === name) {
        this.list[p].destroy();
        this.list.splice(p, 1);
        return { msg: "success" };
      }
    }
    return { err: "Page `"+name+"` does not exists!" };;
  }

//  - -                    - -
// - - - SELECTOR METHODS - - -
//  - -                    - -

  all() {
    let list = [];
    for (let p = 0; p < this.list.length; p++) {
      list.push(this.list[p].data());
    }
    return list;
  }

  /**
   * @method select
   * @memberof PageList
   * @arg {String} page_name Page name
   * @returns {Object} Minimized page data
   */
  select(page_name) {
    for (let p = 0; p < this.list.length; p++) {
      let pdata = this.list[p].data();
      if (pdata.name === page_name) {
        return pdata;
      }
    }
  }

  /**
   * @method select_obj
   * @memberof PageList
   * @arg {String} page_name Page name
   * @return {Page} Page object
   */
  select_obj(page_name) {
    for (let p = 0; p < this.list.length; p++) {
      let pdata = this.list[p];
      if (pdata.name === page_name) {
        return this.list[p];
      }
    }
  }

  add_extra_context(ctx) {
    this.context_extras.push(ctx);
  }
}
