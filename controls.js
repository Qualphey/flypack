

import { Server } from 'socket.io'
const socket_io = Server;
import cookie from 'cookie'

export default class {
  constructor(cfg, cms) {
    let command_path = cfg.command_path;
    let auth = cfg.auth;
    let pages_io = cms.pages;

    let err_response = function(res, text) {
      res.send(JSON.stringify({
        err: text
      }));
    }


    let io = socket_io(cms.app.http_server, {
      path: "/pagepack-socket.io"
    });

    io.use(auth.orize_socket);
    this.sockets = [];
    let _this = this;
    io.use(function(socket, next) {
      _this.sockets.push(socket);

      socket.on("toggle_watch_state", function(data) {
        if (data.id.list) {
          for (let l = 0; l < pages_io.page_lists.length; l++) {
            let cpage_list = pages_io.page_lists[l];
            if (cpage_list.full_path == data.id.list) {
              for (let p = 0; p < cpage_list.list.length; p++) {
                let cpage = cpage_list.list[p];
                if (cpage.full_path == data.id.dirname) {
                  cpage.pack.toggle_watch(data.what);
                }
              }
            }
          }
        } else {
          for (let p = 0; p < pages_io.list.length; p++) {
            let cpage = pages_io.list[p];
            if (cpage.full_path == data.id.dirname) {
              cpage.pack.toggle_watch(data.what);
            }
          }
        }
      });

      socket.on("toggle_watch_state_globals", function(data) {
        for (let p = 0; p < pages_io.globals.length; p++) {
          let cpage = pages_io.globals[p];
          if (cpage.dirname == data.id.dirname) {
            cpage.pack.toggle_watch(data.what);
          }
        }
      });

      next();
    });


    cms.app.post(command_path, auth.orize, function(req, res) {
      var data = JSON.parse(req.body.data);
      /*
        {
          command: "add"|"rm",
          name: "string" - needed on `add` and `rm` commands
        }
      */
      switch (data.command) {
        case 'select':

          switch (data.method) {
            case "all":
              res.send(JSON.stringify(pages_io.all()));
              break;
            default:
              res.send(JSON.stringify({ err: "Invalid selection method: "+data.method }));
          }
        break;
        case 'select_pages':
          switch (data.method) {
            case "all":
              res.send(JSON.stringify( pages_io.all() ));
              break;
            case "name":
              if (data.name && data.name != '') {
                res.send(JSON.stringify( pages_io.select(data.name) ));

              } else {
                res.send(JSON.stringify({ err: "Name parameter missing" }));
              }
              break;
            case "all_from_list":
              if (data.list && data.list != '') {
                res.send(JSON.stringify( pages_io.all_from_list(data.list) ));
              } else {
                res.send(JSON.stringify({ err: "List parameter missing" }));
              }
              break;
            case "name_from_list":
              if (data.list && data.list != '' && data.name && data.name != '') {
                res.send(JSON.stringify( pages_io.select_from_list(data.list, data.name) ));
              } else {
                res.send(JSON.stringify({ err: "List/Name parameter missing" }));
              }
              break;
            default:
              res.send(JSON.stringify({ err: "Invalid selection method: "+data.method }));
          }
          break;
        case 'select_globals':
          switch (data.method) {
            case "all":
              res.send(JSON.stringify( pages_io.all_globals() ));
              break;
            default:
              res.send(JSON.stringify({ err: "Invalid selection method: "+data.method }));
          }
          break;
        case 'add':
          if (data.name && data.name != '' && data.list && data.list != '') {
            let taget_list = pages_io.select_list_obj(data.list);
            res.send(JSON.stringify( taget_list.add(data.name) ));
          } else {
            res.send(JSON.stringify({ err: "Name/List parameter missing" }));
          }
          break;
        case 'rm':
          if (data.name && data.name != '' && data.list && data.list != '') {
            let taget_list = pages_io.select_list_obj(data.list);
            res.send(JSON.stringify( taget_list.remove(data.name) ));
          } else {
            res.send(JSON.stringify({ err: "Name/List parameter missing" }));
          }
          break;/*
        case 'webpack':
          if (data.name) {
            var page = this_class.select_by_name(data.name);
            page.watching = page.compiler.run((err, stats) => {
              if (err) console.error(err);
              // Print watch/build result here...
              res.send(true);
            });
          } else {
            res.send("page name not defined");
          }
          break;*/
        case 'webpack-watch':

          if (data.list && data.name) {
            let page = pages_io.select_obj(data.list, data.name);
            page.webpack_watch();
          } else {
            res.send(JSON.stringify({ err: "Page/List name not defined" }));
          }

          /*
          let jwt_token = undefined;
          if (req.headers.cookie) {
            var cookies = cookie.parse(req.headers.cookie);
            if (cookies['access_token']) {
              jwt_token = cookies['access_token'];
            }
          }
          if (data.list && data.name) {
            let page = pages_io.select_obj(data.list, data.name);
            if (!page.watching) {
              page.webpack_watch(function(stats) {
                cms.router.clients.forEach(client => {
                  if (jwt_token === client.jwt) {
                    if (stats.hasErrors()) {
                      client.socket.emit("webpack-err", stats.toString());
                    } else {
                      client.socket.emit("webpack-done", stats.toString());
                    }
                  }
                });
              });
              res.send(true);
            } else if (page.watching) {
              page.webpack_stop_watching(function() {
                res.send(false);
              });
            }
          } else {
            res.send(JSON.stringify({ err: "Page/List name not defined" }));
          }*/
          break;
        default:
          console.error("PagesIO: unknown command", data.command);
      }
    });
  }

  update_watch_state(what, state, id_obj) {
    for (let s = 0; s < this.sockets.length; s++) {
      let socket = this.sockets[s];
      socket.emit("update_watch_state", {
        what: what,
        state: state,
        id: id_obj
      });
    }
  }

  update_watch_state_globals(what, state, id_obj) {
    for (let s = 0; s < this.sockets.length; s++) {
      let socket = this.sockets[s];
      socket.emit("update_watch_state_globals", {
        what: what,
        state: state,
        id: id_obj
      });
    }
  }
}
