

import express from "express"

import fs from "fs"
import path from "path"

import nunjucks from "nunjucks"
import less from "less"
import csso from 'csso'

import Web from "./web/index.js"

/**
 * Pack class for compiling less and es
 * @class Pack
 * @arg {String} dirname Full path to the environment directory
 * @arg {Object} PagePack express-pagepack object
 * @arg {Object} cfg Configuration
 * @arg {String} cfg.app_path Full path to the Mellisuga project directory
 * @arg {String} cfg.globals_path Path to globals direcory
 * @arg {String} cfg.modules_path Path to modules direcory
 * @arg {Boolean} cfg.dev_mode development mode
 * @arg {Object} cfg.aliases webpack module aliases, `resolve.alias` in configuration object
 *
 */
export default class {

  constructor(dirname, pages_io, cfg) {
    let _this = this;
    this.watch = {};
    this.less_imports = [];
    this.watch_state = {};
    this.context = false;
    this.dirname = dirname;
    this.less_path = path.resolve(dirname, "theme.less");
    this.context_path = path.resolve(dirname, "context.json");
    this.cfg = cfg;
    this.pages_io = pages_io;
    this.page_list = cfg.page_list;
    this.global = cfg.global;
    this.dev_mode = cfg.dev_mode;

    if (cfg.webpack_src && cfg.webpack_out) {
  /*    console.log("PACK", cfg.webpack_src);
        cfg.full_path = dirname;
        this.web = new Web(cfg);
//        this.update_watch_state("webpack", true);
        this.web.webpack_watch();
*/
    } else {
      if (fs.existsSync(path.resolve(dirname, "src/index.js"))) {
        let webcfg = {
          full_path: dirname,
          app_path: cfg.app_path,
          globals_path: cfg.globals_path,
          modules_path: cfg.modules_path,
          dev_mode: cfg.dev_mode,
          aliases: cfg.aliases,
          context: this.context,
          webpack_src: cfg.webpack_src,
          legacy: cfg.legacy
        }
        this.web = new Web(webcfg);
      } else {
        this.web = {
          webpack_watch: function() {
            this.watching = true;
          },
          webpack_stop_watching: function() {
            this.watching = false;
          }
        };
      }

      if (!cfg.reused) {
        fs.watchFile(this.context_path, { interval: 2000 }, function(curr, prev) {
          _this.render_context();
        });
        this.render_context();
      }
    }
  }

  render_context() {
    let _this = this;
    let context_path = this.context_path;
 //   console.log("RENDER CTX", context_path);
    let cfg = this.cfg;
    if (fs.existsSync(context_path)) {
      let context_njk = fs.readFileSync(context_path, 'utf8');
      if (!cfg.context) cfg.context = {};
      let context_src = nunjucks.renderString(context_njk, cfg.context);

      try {
        _this.context = JSON.parse(context_src);
      } catch(ex) {
        _this.context = false;
      }


      if (_this.context.watch) {
        _this.watch_state["less"] = _this.context.watch.less;
        if (_this.context.watch.less && !_this.watch.less) {
          _this.update_watch_state("less", _this.context.watch.less);
          _this.watch_less();
        } else if (!_this.context.watch.less && _this.watch.less) {
          _this.update_watch_state("less", _this.context.watch.less);
          _this.stop_watch_less();
        }

        _this.watch_state["webpack"] = _this.context.watch.webpack;
        if (_this.context.watch.webpack && !_this.web.watching) {
          _this.update_watch_state("webpack", _this.context.watch.webpack);
          _this.web.webpack_watch();
        } else if (!_this.context.watch.webpack && _this.web.watching) {
          _this.update_watch_state("webpack", _this.context.watch.webpack);
          _this.web.webpack_stop_watching();
        }
      } else {
        _this.watch_state = {
          less: false,
          webpack: false
        }
        _this.write_watch_states(false, false)
      }
    }
  }

  render_less() {
    let _this = this;
    let file = this.less_path;
    if (fs.existsSync(file)) {
      let less_src = nunjucks.renderString(fs.readFileSync(file, 'utf8'), this.context);
      less.render(less_src, {
        relativeUrls: true,
        paths: [this.dirname]
      }, function(err, output) {
        let report_time = new Date();
        report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);
        if (err) {
          console.error("");
          console.error("\x1b[34m"+report_time+" \x1b[31mLESS COMPILATION ERROR:")
          console.error("\x1b[33m"+err.message+" --- \x1b[32mline: "+err.line+"\x1b[0m");
          console.error("\x1b[36m");
          for (let e = 0; e < err.extract.length; e++) {
            if (err.extract[e]) console.error(err.extract[e]);
          }
          console.error("\x1b[0m");
        } else {
          let output_css = output.css;
          if (!_this.dev_mode) output_css = csso.minify(output_css).css;
          fs.writeFileSync(file+".css", output_css);
          console.log("\x1b[34m"+report_time, "\x1b[1m\x1b[32mLESS\x1b[0m ----->\x1b[36m", file+".css\x1b[0m");
        }
      });
    }
  }

  watch_less() {
    let _this = this;
    if (fs.existsSync(this.less_path)) {
      const less_file = fs.readFileSync(this.less_path, 'utf8');
      const imports = less_file.match(/(?<=@import ")(.*)(?=";)/g);
      if (imports != null) {
        for (const impo of imports) {
          const import_path = path.resolve(path.dirname(this.less_path), impo);
          const less_import = {
            path: import_path
          };
          less_import.watch = fs.watchFile(import_path, { interval: 1000 }, function(curr, prev) {
            _this.render_less();
          });
          _this.less_imports.push(less_import);
        }
      }
    }

    this.watch.less = fs.watchFile(this.less_path, { interval: 1000 }, function(curr, prev) {
      _this.render_less();
    });
    this.render_less();
  }

  stop_watch_less() {
    if (this.watch.less) fs.unwatchFile(this.less_path);
    for (const impo of this.less_imports) {
      fs.unwatchFile(impo.path);
    }
    this.less_imports = [];
    this.watch.less = false;
  }

  toggle_webpack_watch() {
    if (fs.existsSync(this.context_path)) {
      let context_njk = fs.readFileSync(this.context_path, 'utf8');
      let context_src = nunjucks.renderString(context_njk, this.cfg.context);

      let cur_context = false;
      try {
        cur_context = JSON.parse(context_src);
      } catch(ex) {
        console.error(ex.stack);
        cur_context = false;
      }

      if (!cur_context.watch) cur_context.watch = { webpack: true };
      cur_context.watch.webpack = !cur_context.watch.webpack;

      fs.writeFileSync(this.context_path, JSON.stringify(cur_context, null, 2));
    }
  }

  toggle_watch(what) {
    if (fs.existsSync(this.context_path)) {
      let context_njk = fs.readFileSync(this.context_path, 'utf8');
      let context_src = nunjucks.renderString(context_njk, this.cfg.context);

      let cur_context = false;
      try {
        cur_context = JSON.parse(context_src);
      } catch(ex) {
        console.error(ex.stack);
        cur_context = false;
      }

      if (!cur_context.watch) cur_context.watch = {};
      cur_context.watch[what] = !cur_context.watch[what];

      fs.writeFileSync(this.context_path, JSON.stringify(cur_context, null, 2));
    }
  }

  update_watch_state(what, state) {
    if (this.pages_io.controls) {
      if (this.global) { // TODO implement for globals
        this.pages_io.controls.update_watch_state_globals(what, state, {
          dirname: this.dirname
        });
      } else {
        this.pages_io.controls.update_watch_state(what, state, {
          list: this.page_list ? this.page_list.full_path : false,
          dirname: this.dirname
        });
      }
    }
  }

  write_watch_states(less, webpack) {
    let cur_context = false;

    if (fs.existsSync(this.context_path)) {
      let context_njk = fs.readFileSync(this.context_path, 'utf8');
      let context_src = nunjucks.renderString(context_njk, this.cfg.context);


      try {
        cur_context = JSON.parse(context_src);
      } catch(ex) {
        console.log("ERROR IN:", this.context_path);
        console.error(ex.stack);
       // cur_context = {};
      }
    } else {
      cur_context = {};
    }

    if (cur_context) {
      cur_context.watch = {
        less: less,
        webpack: webpack
      };

      fs.writeFileSync(this.context_path, JSON.stringify(cur_context, null, 2));
    }
  }
}
