


import webpack from 'webpack'
//const MinifyPlugin = require("babel-minify-webpack-plugin");
import TerserPlugin from "terser-webpack-plugin"

import fs from "fs"
import path from "path"

import { createRequire } from 'module';

const require = createRequire(import.meta.url);

import { URL } from 'url'; // in Browser, the URL in native accessible on window

const __filename = new URL('', import.meta.url).pathname;
// Will contain trailing slash
const __dirname = new URL('.', import.meta.url).pathname;

/**
 * Webpack compiler class
 * @class Web
 * @arg {Object} cfg Configuration
 * @arg {String} cfg.full_path Full path to the environment directory
 * @arg {String} cfg.app_path Full path to the Mellisuga project directory
 * @arg {String} cfg.globals_path Path to globals direcory
 * @arg {String} cfg.modules_path Path to modules direcory
 * @arg {Boolean} cfg.dev_mode development mode
 * @arg {Object} cfg.aliases webpack module aliases, `resolve.alias` in configuration object
 * @arg {Object} cfg.context context object for watch options
 *
 */
export default class {

  constructor(cfg) {

    this.cfg = cfg;
    this.full_path = cfg.full_path;
    this.rel_path = "./"+cfg.full_path.replace(cfg.app_path+'/', '');
    
    const modules_path = cfg.modules_path;

    let this_class = this;
    let _this = this;

    let aliases = {
      globals: path.resolve(cfg.app_path, 'globals/modules'),
      core: path.resolve(cfg.app_path, 'mellisuga_modules/web/core'),
      community: path.resolve(cfg.app_path, 'mellisuga_modules/web/community')
    }

    if (cfg.aliases) {
      for (let alias in cfg.aliases) {
        aliases[alias] = cfg.aliases[alias];
      }
    }

    let webpack_plugins = [
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
        process: ['process'],
      })
    ];

//    if (!cfg.dev_mode) webpack_plugins.push(new MinifyPlugin());
    let webpack_optimization = {};

    if (!cfg.dev_mode) {
      webpack_optimization.minimize = true;
      webpack_optimization.minimizer = [new TerserPlugin()];
    }

    this.context = cfg.context;

    let entries = []

    const babel_env = {
      "useBuiltIns": "usage",
      "corejs": { version: "3.8", proposals: true }
    }

    const pack_entry = {};
    if (!cfg.legacy) {
      pack_entry['./main'] = [this.full_path+'/src/index.js'];
      babel_env.targets = {
        browsers: [
          'Chrome >= 60',
          'Safari >= 10.1',
          'iOS >= 10.3',
          'Firefox >= 54',
          'Edge >= 15',
        ]
      };
    } else {
      pack_entry['./main.es5'] = [this.full_path+'/src/index.js'];
    }

    this.compiler = webpack({
  /*      watch: true,
        watchOptions: {
          aggregateTimeout: 300,
          poll: 1000
        },*/
        mode: 'development',
        entry: pack_entry,
        output: {
          path: this.full_path,
          filename: '[name].js'
        },
        plugins: webpack_plugins,
        optimization: webpack_optimization,
        resolve: {
          modules: [
            path.resolve(cfg.app_path, 'node_modules'), modules_path
          ],
          fallback: {
     //       assert: require.resolve("assert"),
            buffer: require.resolve("buffer/"),
//            console: require.resolve("console-browserify"),
//            constants: require.resolve("constants-browserify"),
            crypto: require.resolve("crypto-browserify/"),
//            domain: require.resolve("domain-browser"),
            events: require.resolve("events/"),
//            http: require.resolve("stream-http"),
//            https: require.resolve("https-browserify"),
//            os: require.resolve("os-browserify/browser"),
            path: require.resolve("path-browserify/"),
            punycode: require.resolve("punycode/"),
            process: require.resolve("process/browser"),
//            querystring: require.resolve("querystring-es3"),
            stream: require.resolve("stream-browserify/"),
//            _stream_duplex: require.resolve("readable-stream/duplex"),
//            _stream_passthrough: require.resolve("readable-stream/passthrough"),
            _stream_readable: require.resolve("readable-stream/readable"),
//            _stream_transform: require.resolve("readable-stream/transform"),
//            _stream_writable: require.resolve("readable-stream/writable"),
            string_decoder: require.resolve("string_decoder/"),
            sys: require.resolve("util/"),
//            timers: require.resolve("timers-browserify"),
//            tty: require.resolve("tty-browserify"),
            url: require.resolve("url"),
            util: require.resolve("util/"),
//            vm: require.resolve("vm-browserify"),
            zlib: require.resolve("browserify-zlib"),
            module: false
          },
          alias: aliases 
        },
        module: {
            rules: [
              {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: [
                      ['@babel/preset-env', babel_env ]
                    ],
                    plugins: ['@babel/plugin-proposal-class-properties']
                  }
                }
              },
              { test: /\.css$/, use: 'css-loader' },
              {
                test: /\.less$/i,
                use: [
                  // compiles Less to CSS
                  "style-loader",
                  "css-loader",
                  "less-loader",
                ],
              },
              {
                test: /\.(html)$/,
                use: {
                  loader: 'html-loader',
                  options: {
                    esModule: false
                  }
                }
              },
              {
                test: /\.(vs|fs)$/,
                use: {
                  loader: 'raw-loader'
                }
              }
            ]
        },
        stats: {
          colors: true
        },
        devtool: 'source-map'
    });

    if (cfg.autowatch || (this.context && this.context.watch && this.context.watch.webpack)) {
      fs.writeFileSync(path.resolve(this_class.full_path, "errors.log"), "Watching since: "+(new Date())+"\n");
      this.watching = this.compiler.watch({
        aggregateTimeout: 100,
        poll: 100
      }, (err, stats) => {
        if (err) console.error(err);
        _this.webpack_report(stats);
      });
    }

  }

  webpack_watch(next) {
//    console.log("WATCH", this.full_path);
    let _this = this;
    this.watching = this.compiler.watch({
      aggregateTimeout: 100,
      poll: 100
    }, (err, stats) => {
      if (err) console.error(err);
      _this.webpack_report(stats);
      if (typeof next === "function") next(stats);
    });
 }

  webpack_stop_watching(next) {
    let this_class = this;
    if (this.watching) {
      this.watching.close(() => {
        this_class.watching = undefined;
        if (typeof next === "function") next();
      });
      this.watching = false;
    }
  }



  webpack_report(stats) {
    let report_time = new Date();
    report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);

    if (stats.hasErrors()) {
      let info = stats.toJson();
      let errors = info.errors;

      for (let e = 0; e < errors.length; e++) {
        let err_ostr = "\n\x1b[34m"+report_time+"\n"+"\x1b[31mERROR IN: \x1b[33m"+errors[e].moduleName+"\x1b[0m\n";
        err_ostr += errors[e].stack;

        /*
        let err_lines = errors[e].split("\n");
        err_lines[0] = "\x1b[34m"+report_time+"\n"+"\x1b[31mERROR: \x1b[33m"+err_lines[0]+"\x1b[0m";

        for (let l = 0; l < err_lines.length; l++) {
          err_ostr += err_lines[l]+"\n";
        }
        */
        console.log(err_ostr);
        let err_ostr_nocolor = err_ostr.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '');
        fs.appendFileSync(path.resolve(this.full_path, "errors.log"), err_ostr_nocolor);
      }
    } else {
      console.log("\x1b[34m"+report_time, "\x1b[1m\x1b[36mWEBPACK\x1b[0m -->", "\x1b[36m"+this.full_path+"/main.js\x1b[0m");
      if (stats.hasWarnings()) {
        let info = stats.toJson();
        let errors = info.warnings;
        
        for (let e = 0; e < errors.length; e++) {
          let err_lines = errors[e].details.split("\n");
          err_lines[0] = "\x1b[33mWARNING: "+err_lines[0]+"\x1b[0m";

          let err_ostr = "";
          for (let l = 0; l < err_lines.length; l++) {
            err_ostr += err_lines[l]+"\n";
          }
          console.log(err_ostr);
          let err_ostr_nocolor = err_ostr.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '');
          fs.appendFileSync(path.resolve(this.full_path, "errors.log"), err_ostr_nocolor);
        }
      }
    }
  }
}
